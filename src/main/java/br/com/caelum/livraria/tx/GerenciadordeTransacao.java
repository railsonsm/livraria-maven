package br.com.caelum.livraria.tx;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

@Transactional
@Interceptor
public class GerenciadordeTransacao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	EntityManager manager;
	
	@AroundInvoke //
	public Object executaTx(InvocationContext context) throws Exception {
		//abre transacao
		System.out.println("abrindo conexao");
		manager.getTransaction().begin();
		
		//chama os daos que precisam de um tx
		Object resultado = context.proceed();
		
		//commita a transacao
		System.out.println("comitando conexao");
		manager.getTransaction().commit();
		
		return resultado;
	}
}
